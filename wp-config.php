<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aivemawordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MNdxDMX~pRBh)Z#0bW1~>v0RD+:WNpz~{ght&u=r5cVqd`e,+Bu} |G|[Wn)$2,E');
define('SECURE_AUTH_KEY',  '`&a<,QmOTc1oX g?6m.@GH~}1{Q!USMJEOxnC/I=M9<1.vx/WxUO&fgrfViWW&Ht');
define('LOGGED_IN_KEY',    '`/Gpx3HWXSlVD^.BD[hS7ASkT39rFIc{UN`(3thCzm.zSvy8g$zk:3=-{X`4@1@(');
define('NONCE_KEY',        'f<g/C<WV8%zSyg;RwJBncj,=5P9en yGc_*~q8G!CSs>Tii_ZE:*PC-lQpUV|r@s');
define('AUTH_SALT',        '`-nA*v:t8. m*rohqTDo&8V+D3,4}Pn16ye3hfYMd4r^HD.I_ ;+|On5 rUd41.3');
define('SECURE_AUTH_SALT', 'lCVjq(JWn%cH<Jw6;A.B.3}i>$l291Mx;Z5O<UKoC]Twmlc;Jn `iFxi&s?w=Qa4');
define('LOGGED_IN_SALT',   'vWlC^j:71iixY;$yaDF7b?a6UuO7vkn1T>*uMFc.B)o*f{dpO4#D.{za]GirHC)o');
define('NONCE_SALT',       '$}TuXq7FL;T9]OE,+h%cv><7G,>oP %/<XOw73fp5qOPk@A$4:F`wRL#0@=qtZU;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
